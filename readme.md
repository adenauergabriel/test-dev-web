#Bem-vindo ao teste para desenvolvedor web
Este teste tem como finalidade avaliar as suas habilidades para a posição de desenvolvedor web. 

##Atividades
O teste procura refletir os processos, problemas e desafios enfrentados no desenvolvimento do aplicativo. 

###Pesquisa e desenvolvimento
É comum, em nosso em dia a dia, termos que decompor problemas complexos e pesquisar por soluções simples e elegantes. Usamos Design Thinking, Responsive Design e Progressive Web App para isso.

Desta forma, o seu desafio aqui é criar e aplicar um tema, de sua escolha, para o formulário (patient) e a tabela (patients) do arquivo src/index.html.

Dica: O aplicativo é direcionado à profissionais da área de saúde e é acessado por desktops, tablets e smartphones. 

###Comunicação como o servidor - JSON 
O front end do applicativo não é só web, é mobile também. Android e iPhone.

Assim, o seu desafio aqui é mover os dados do formulario (patient) para a tabela (patients) através de um serviço de backend usando o protocolo JSON.

Dica: Use um serviço de echo, pode ser o http://httpbin.org/post ou qualquer outro de sua preferencia, para simular o back end.

###Versionamento - GIT
O nosso dia a dia é corrido e, geralmente, trabalhamos em mais de uma funcionalidade ao mesmo tempo. Usamos o GIT para isso. 

Assim, o seu desafio aqui é clonar este repositorio, fazer alguns "commits" durante o desenvolvimento do teste e no fim disponibilizar o seu trabalho em outro repositorio. 

Voce poderá publicar o resultado do seu teste no github.com, bitbucket.org ou qualquer outro serviço de sua escolha.   

Para comunicar a conclusão do seu teste, responda o email incluindo a URL do repositório (público) que contém o resultado do seu teste. 
 


